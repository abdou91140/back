<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Booking;
use App\Repository\BookingRepository;
use App\Repository\BookRepository;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use DateTimeImmutable;

#[Route('/api')]
class ApiController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    #[Route('/books', name: 'app_api_books', methods: ['GET'])]
    public function books(
        BookRepository $bookRepository,
        #[MapQueryParameter] array $filter = [],
    ): Response {

        if (!empty($filter)) {
            try {
                $filteredBooks = $bookRepository->findByFilters($filter);
            } catch (\Exception $e) {
                 dump($e);
            }
        } else {
            $filteredBooks = $bookRepository->findAll();
        }

        return JsonResponse::fromJsonString($this->serializer->serialize($filteredBooks, 'json', ['groups' => 'getBooks']));
    }

    #[Route('/books/{id}', name: 'app_api_show', methods: ['GET'])]
    public function show(Book $book): JsonResponse
    {
        return JsonResponse::fromJsonString($this->serializer->serialize($book, 'json'));
    }

    #[Route('/books/bookings', name: 'app_api_create_booking', methods: ['POST'])]
    public function createBooking(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $book = $entityManager->getRepository(Book::class)->findOneBy(["id" => $data['bookId']]);
        $dateStart = DateTimeImmutable::createFromFormat('Y-m-d', $data['formData']['bookingDateStart']);
        $dateEnd = DateTimeImmutable::createFromFormat('Y-m-d', $data['formData']['bookingDateEnd']);
        $booking = new Booking();
        $booking->setBook($book);
        $booking->setStartDate($dateStart);
        $booking->setEndDate($dateEnd);
        $booking->isStatus(true);
        $entityManager->persist($booking);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
