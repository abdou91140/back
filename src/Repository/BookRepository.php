<?php

namespace App\Repository;

use App\Entity\Book;
use DateTime;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;

/**
 * @extends ServiceEntityRepository<Book>
 *
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findByFilters(array $filter): array
    {
        $qb = $this->createQueryBuilder('b');

        foreach ($filter as $field => $value) {
            if (property_exists(Book::class, $field)) {
                if ($field === 'publishedAt') {
                    $qb->orderBy('ABS(DATE_DIFF(b.publishedAt, :date))', 'ASC')
                    ->setParameter('date', $value)
                    ->setMaxResults(2)
                    ->getQuery()->getResult();
                } else {
                    $qb->andWhere($qb->expr()->like("b.$field", ":$field"))
                        ->setParameter($field, '%' . $value . '%');
                }
            } else {
                throw new \InvalidArgumentException("Invalid field name: $field");
            }
        }

        return $qb->getQuery()->getResult();
    }

    //    /**
    //     * @return Book[] Returns an array of Book objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('b.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Book
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }

}
